import requests
import time
import sys

url = f'http://{sys.argv[1]}/actuator/health'
print(url)

for i in range(20):
    response = str(requests.get(url))
    if "200" in response:
        print("Application is up and running")
        break
    else:
        print(f"Application dont response")
        time.sleep(10)
else:
    print("Application is nots running,something went wrong")
    exit(1)
