#Parameter group for RDS-DB
resource "aws_db_parameter_group" "demo4-db-pg-group" {
  name   = "demo4-rds-pgroup"
  family = "mysql8.0"

  parameter {
    name  = "character_set_server"
    value = "utf8"
  }

  parameter {
    name  = "character_set_client"
    value = "utf8"
  }
}

#Subnet-group for RDS-db
resource "aws_db_subnet_group" "demo4-db-subnet-group" {
  name       = "demo4-namiq-db-subnet"
  subnet_ids = module.vpc.private_subnets
  tags = {
    Name = "My DB subnet group"
  }
}
#Security group for RDS-db
resource "aws_security_group" "db-sg" {
  name        = "db-security-group"
  description = "allow inbound access to the database"
  vpc_id      = module.vpc.vpc_id

  ingress {
    protocol    = "tcp"
    from_port   = 3306
    to_port     = 3306
    cidr_blocks = var.cidr_blocks
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}
#RDS-db
resource "aws_db_instance" "demo4-database-db" {
  allocated_storage    = 20
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "8.0"
  instance_class       = var.db_instance_class
  identifier           = var.db_identifier
  name                 = var.db_name
  username             = var.db_user
  password             = var.db_pass
  parameter_group_name = aws_db_parameter_group.demo4-db-pg-group.id
  db_subnet_group_name = aws_db_subnet_group.demo4-db-subnet-group.id
  vpc_security_group_ids = [ aws_security_group.db-sg.id ]
  publicly_accessible  = false
  skip_final_snapshot  = true
  
}


