### VPC variables
variable "vpc_cidr" {
  type        = string
  default     = "10.0.0.0/16"
  description = "CIDR for VPC"
}

variable "vpc-private-subnets" {
  default = ["10.0.7.0/24", "10.0.8.0/24", "10.0.9.0/24"]
}

 variable "vpc-public-subnets" {
  default = ["10.0.10.0/24", "10.0.11.0/24", "10.0.12.0/24"]
}

variable "enable_dns_support" {
    default = true
}

variable "enable_dns_hostnames" {
    default = true
}
variable "enable_nat_gateway" {
    default = true
}

variable "vpc_name" {
  type        = string
  default     = "demo4-vpc-eks"
  description = "vpc-for-demo4-eks"
}

variable "aws_region" {
  default     = "eu-west-1"
  description = ""
}



## worker group variables 1

variable "worker-group1" {
  default = "worker-group-1"
}

variable "worker-instance-type1" {
  default = "t2.small"
}

variable "worker-asg-capacity1" {
  default = 2
}

## worker group variables 2

variable "worker-name2" {
  default = "worker-group-2"
}

variable "worker-type2" {
  default = "t2.medium"
}

variable "worker-asg-capacity2" {
  default = 1
}


#### RDS variables

variable "db_instance_class" {
  default = ""
}
variable "db_identifier" {
  default = ""
}

variable "db_name" {
  default = ""
}

variable "db_user" {
  default = ""
}

variable "db_pass" {
  default = ""
}

variable "db_port" {
  default = ""
}

#cidr blocks
variable "cidr_blocks" {
  
  default = ["10.0.0.0/8","192.168.0.0/16","172.16.0.0/12"]
}
