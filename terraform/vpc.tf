provider "aws" {
  region = var.aws_region
}

locals {
    cluster_name = "demo4-eks-cluster"
 }

data "aws_availability_zones" "available" {}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  version = "2.66.0"

  name = var.vpc_name
  cidr = var.vpc_cidr
  azs             = data.aws_availability_zones.available.names
  private_subnets = var.vpc-private-subnets
  public_subnets  = var.vpc-public-subnets
  enable_nat_gateway = var.enable_nat_gateway
  enable_dns_support = var.enable_dns_support
  enable_dns_hostnames = var.enable_dns_hostnames
}
